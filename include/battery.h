enum battery_status {
	BATTERY_UNKNOWN,
	BATTERY_NOT_FOUND,
	BATTERY_ERRNO,
	BATTERY_UNPLUGGED,
	BATTERY_OK
};

extern enum battery_status battery_open(void);
extern enum battery_status battery_get(unsigned char*);
extern enum battery_status battery_close(void);
